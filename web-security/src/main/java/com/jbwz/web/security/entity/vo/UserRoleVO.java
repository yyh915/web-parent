package com.jbwz.web.security.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户角色
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserRoleVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 角色id
     */
    private Long roleId;
}
