package com.jbwz.web.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 资源
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_s_resource")
public class Resource extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 资源名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 资源标识
     */
    private String authKey;

    /**
     * 父id
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 1:菜单，2：按钮
     */
    @TableField("`type`")
    private Integer type;

    /**
     * 路径
     */
    private String path;

    private String dependencyPath;

    /**
     * 描述
     */
    private String descriptions;
    /**
     * 排序
     */
    private Integer menuSort;

    /**
     * 0：启用，1：禁用
     */
    @TableField("`status`")
    private Integer status;


}
