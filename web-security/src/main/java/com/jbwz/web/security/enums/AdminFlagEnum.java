package com.jbwz.web.security.enums;

import com.jbwz.web.common.base.BaseEnum;
import lombok.Getter;

public enum AdminFlagEnum implements BaseEnum {
    SUPER_ADMIN(1, "超级管理员"),
    ADMIN(2, "管理员"),
    NORMAL(3, "一般用户"),
    ;
    @Getter
    private Integer code;
    private String msg;

    AdminFlagEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}