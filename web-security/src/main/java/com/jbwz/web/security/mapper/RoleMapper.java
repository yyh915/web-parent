package com.jbwz.web.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.security.entity.Role;
import com.jbwz.web.security.entity.vo.RoleVO;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
public interface RoleMapper extends BaseMapper<Role> {

    IBasePage<RoleVO> pageList(IBasePage page, RoleVO vo);
}
