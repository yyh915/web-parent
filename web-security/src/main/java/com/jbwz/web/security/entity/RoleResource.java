package com.jbwz.web.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 角色资源关联
 * </p>
 *
 * @author yyh
 * @since 2021-07-18 22:02:17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_s_role_resource")
public class RoleResource extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("role_id")
    private Long roleId;

    @TableField("resource_id")
    private Long resourceId;


}
