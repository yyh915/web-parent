package com.jbwz.web.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.security.entity.User;
import com.jbwz.web.security.entity.vo.UserVO;

/**
 * 用户 Mapper 接口
 *
 * @author yyh
 * @since 2021-07-15 22:48:24
 */
public interface UserMapper extends BaseMapper<User> {

    IBasePage<UserVO> pageList(IBasePage page, UserVO vo);
}
