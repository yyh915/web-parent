package com.jbwz.web.security.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 资源
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ResourceVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 资源名称
     */
    @Length(min = 2, max = 32)
    @NotNull
    private String name;

    /**
     * 资源标识
     */
    @Length(min = 2, max = 128)
    @NotNull
    private String authKey;

    /**
     * 父id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long parentId;

    /**
     * 1:菜单，2：按钮,3:功能
     */
    @Max(3)
    @Min(1)
    @NotNull
    private Integer type;

    /**
     * api路径
     */
    @Length(min = 2, max = 255)
    private String path;

    /**
     * 依赖其它的api路径
     */
    @Length(min = 2, max = 512)
    private String dependencyPath;

    /**
     * 描述
     */
    @Length(max = 512)
    private String descriptions;
    /**
     * 排序
     */
    private Integer menuSort;

    /**
     * 0：启用，1：禁用
     */
    private Integer status;

    List<ResourceVO> children;
}
