package com.jbwz.web.security.enums;

import com.jbwz.web.common.base.BaseEnum;
import lombok.Getter;

public enum ResourceTypeEnum implements BaseEnum {
    MENU(1, "菜单"),
    BUTTON(2, "按钮"),
    ;
    @Getter
    private Integer code;
    private String msg;

    ResourceTypeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}