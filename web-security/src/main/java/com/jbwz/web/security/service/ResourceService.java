package com.jbwz.web.security.service;

import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.session.SessionUser;
import com.jbwz.web.security.entity.Resource;
import com.jbwz.web.security.entity.vo.ResourceVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 资源 服务类
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:44:43
 */
public interface ResourceService extends BaseService<Resource> {


    List<ResourceVO> listByPid(Long pid);

    List<ResourceVO> listByPidWithChildren(Long pid);

    List<ResourceVO> listMenuTree();

    void saveOrUpdateWithCheck(Resource e);

    /**
     * 获取当前用户的权限资源
     *
     * @param user
     * @return
     */
    List<Resource> getCurrentUserResource(SessionUser user);

    /**
     * 获取当前用户的权限key，界面使用
     *
     * @param user
     * @return
     */
    Set<String> getCurrentUserResourcePkey(SessionUser user);
}
