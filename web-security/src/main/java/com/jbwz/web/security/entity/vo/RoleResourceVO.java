package com.jbwz.web.security.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 角色资源关联
 *
 * @author yyh
 * @since 2021-07-19 22:10:05
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RoleResourceVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roleId;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long resourceId;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private List<Long> resourceIds;
}
