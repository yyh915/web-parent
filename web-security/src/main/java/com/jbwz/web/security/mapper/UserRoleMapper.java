package com.jbwz.web.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jbwz.web.security.entity.UserRole;

/**
 * <p>
 * 用户角色 Mapper 接口
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
