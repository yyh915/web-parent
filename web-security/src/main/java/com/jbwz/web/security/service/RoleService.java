package com.jbwz.web.security.service;

import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.security.entity.Role;
import com.jbwz.web.security.entity.vo.RoleVO;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:44:43
 */
public interface RoleService extends BaseService<Role> {

    IBasePage pageList(IBasePage page, RoleVO vo);

    void saveOrUpdateVO(RoleVO vo);
}
