package com.jbwz.web.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jbwz.web.security.entity.Resource;
import com.jbwz.web.security.entity.RoleResource;

import java.util.List;

/**
 * <p>
 * 角色资源关联 Mapper 接口
 * </p>
 *
 * @author yyh
 * @since 2021-07-18 22:02:17
 */
public interface RoleResourceMapper extends BaseMapper<RoleResource> {

    List<Resource> listByResourceByRoleIds(List<Long> rids);

}
