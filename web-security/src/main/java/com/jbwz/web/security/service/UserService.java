package com.jbwz.web.security.service;

import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.session.SessionUser;
import com.jbwz.web.security.entity.User;
import com.jbwz.web.security.entity.vo.UserVO;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Set;

/**
 * 用户 服务类
 *
 * @author yyh
 * @since 2021-07-16 23:43:12
 */
public interface UserService extends BaseService<User>, UserDetailsService {

    IBasePage pageList(IBasePage page, UserVO vo);


    RequestMatcher getCurrentUserResourceMatcher(SessionUser user);

    Set<String> getCurrentUserPKey();

    void saveByReg(User user);

    void saveOrUpdateVO(UserVO vo);

    void removeByIdCheck(Long id);
}
