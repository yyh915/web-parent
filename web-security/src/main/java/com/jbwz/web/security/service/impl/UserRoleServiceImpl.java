package com.jbwz.web.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.security.entity.UserRole;
import com.jbwz.web.security.mapper.UserRoleMapper;
import com.jbwz.web.security.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色 服务实现类
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:44:43
 */
@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {


    @Override
    public void delByUserId(Long id) {
        //todo 验证update条件组成,及更新组成
        UserRole userRole = new UserRole();
        userRole.setUserId(id);
        UpdateWrapper<UserRole> uw = Wrappers.update(userRole);
        baseMapper.delete(uw);
    }

    @Override
    public List<Long> getByUserId(Long id) {
        UserRole userRole = new UserRole();
        userRole.setUserId(id);
        QueryWrapper<UserRole> qw = Wrappers.query(userRole);
        return baseMapper.selectList(qw).stream().map(UserRole::getRoleId).toList();
    }

}
