package com.jbwz.web.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jbwz.web.common.session.SessionUser;
import com.jbwz.web.security.entity.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 资源 Mapper 接口
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
public interface ResourceMapper extends BaseMapper<Resource> {

  List<Resource> getCurrentUserResource(
      @Param("userId") Long userId, @Param("onlyMenu") Boolean onlyMenu);
}
