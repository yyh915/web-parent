package com.jbwz.web.security.service;

import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.security.entity.RoleResource;
import com.jbwz.web.security.entity.vo.MenuTreeVO;
import com.jbwz.web.security.entity.vo.RoleResourceVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色资源关联 服务类
 * </p>
 *
 * @author yyh
 * @since 2021-07-18 22:02:17
 */
public interface RoleResourceService extends BaseService<RoleResource> {

    IBasePage pageList(IBasePage page, RoleResourceVO vo);

    void removeByRoleId(Long roleId);

    Set<Long> listByRoleId(Long roleId);

    MenuTreeVO listByRVoByRoleIds(List<Long> currentRoleIds);
}
