package com.jbwz.web.security.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@Data
@ConfigurationProperties(prefix = "project.security")
public class SecurityUrlProperties {
    /**
     * 不需要登录就能访问的url
     */
    private List<String> permitAll = new ArrayList<>();
    /**
     * 只要登录就能访问的url,不需要权限校验`
     */
    private List<String> authOnly = new ArrayList<>();
}
