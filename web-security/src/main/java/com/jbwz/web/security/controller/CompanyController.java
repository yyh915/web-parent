package com.jbwz.web.security.controller;


import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import com.jbwz.web.common.validated.InsertGroup;
import com.jbwz.web.security.entity.Company;
import com.jbwz.web.security.entity.vo.CompanyVO;
import com.jbwz.web.security.service.CompanyService;
import com.jbwz.web.security.service.biz.CompanyUserBiz;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 公司 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@RestController
@RequestMapping("/company")
public class CompanyController extends BaseController {

    @Autowired
    CompanyService companyService;
    @Autowired
    CompanyUserBiz companyUserBiz;

    @GetMapping("/page-list")
    public ResponseJson pageList(BasePage page, CompanyVO vo) {
        IBasePage result = companyService.pageList(page, vo);
        return success(result);
    }

    @PostMapping("/reg")
    public ResponseJson reg(@RequestBody @Validated({InsertGroup.class}) CompanyVO vo) {
        companyUserBiz.reg(vo);
        return success();
    }

    @PostMapping("/save")
    public ResponseJson save(@RequestBody @Validated CompanyVO vo) {
        companyService.saveValidate(vo);
        return success();
    }

    @GetMapping("/del/{id}")
    public ResponseJson del(@PathVariable("id") Long id) {
        companyService.removeById(id);
        return success();
    }

    @GetMapping("/detail/{id}")
    public ResponseJson detail(@PathVariable("id") Long id) {
        Company e = companyService.getById(id);
        return success(toVO(e));
    }

    private CompanyVO toVO(Company e) {
        CompanyVO vo = new CompanyVO();
        BeanUtils.copyProperties(e, vo);
        return vo;
    }

}
