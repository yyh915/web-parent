package com.jbwz.web.security.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import com.jbwz.web.common.validated.InsertGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 公司
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CompanyVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    @Length(max = 255)
    @NotNull
    private String name;
    @Length(max = 32)
    @NotNull(groups = InsertGroup.class)
    private String username;

    @Length(min = 3, max = 16)
    @NotNull(groups = InsertGroup.class)
    private String password;
    private String logoUrl;

    /**
     * 创建人id
     */
    private Long createdId;

    /**
     * 创建人名称
     */
    @Length(max = 512)
    private String createdName;
}
