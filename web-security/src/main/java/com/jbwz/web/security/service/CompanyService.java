package com.jbwz.web.security.service;

import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.security.entity.Company;
import com.jbwz.web.security.entity.vo.CompanyVO;

import java.util.Optional;

/**
 * <p>
 * 公司 服务类
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:44:42
 */
public interface CompanyService extends BaseService<Company> {

    IBasePage pageList(IBasePage page, CompanyVO vo);

    Company saveValidate(CompanyVO vo);

    Optional<Company> getByName(String name);
}
