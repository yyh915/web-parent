package com.jbwz.web.security.login;

import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.res.ResponseJson;
import com.jbwz.web.security.common.ResponseCodeSecurity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public class LoginController extends BaseController {
  /**
   * 未登录会，跳转到这
   *
   * @return
   */
  @RequestMapping("/login")
  public ResponseJson loginGet() {
    return fail(ResponseCodeSecurity.LOGIN_INACTIVE);
  }

  @RequestMapping("/logout")
  public ResponseJson logout() {
    return success();
  }

  /**
   * 登录失败，跳转到这
   *
   * @return
   */
  @RequestMapping("/login-error")
  public ResponseJson loginError(HttpServletRequest request) {
    log.debug("登录错误>>参数为：{}", request.getParameterMap());
    request.getSession().invalidate();
    return fail(ResponseCodeSecurity.LOGIN_ERROR);
  }

  /**
   * 登录超时失效，跳转到这
   *
   * @param request
   * @return
   */
  @RequestMapping("/login-expired")
  public ResponseJson loginExpired(HttpServletRequest request) {
    request.getSession().invalidate();
    return fail(ResponseCodeSecurity.LOGIN_EXPIRED);
  }

  /**
   * 登录成功会后，跳转到这
   *
   * @param request
   * @return
   */
  @RequestMapping("/login-success")
  public ResponseJson loginPost(HttpServletRequest request) {
    return success();
  }
}
