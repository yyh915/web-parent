package com.jbwz.web.security.entity.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 菜单树
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
public class MenuTreeVO {

    private static final long serialVersionUID = 1L;
    @Length(max = 512)
    private String pid;
    @Length(max = 512)
    private String key;
    @Length(max = 512)
    private String title;
    private List<MenuTreeVO> children = new ArrayList<>();
}
