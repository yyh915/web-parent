package com.jbwz.web.security.common;

import com.jbwz.web.common.res.IResponseCodeEnum;
import lombok.Getter;

@Getter
public enum ResponseCodeSecurity implements IResponseCodeEnum {
  PWD_WRONG(2001, "密码错误"),
  LOGIN_INACTIVE(2002, "未登录"),
  LOGIN_ERROR(2003, "登录异常"),
  LOGIN_EXPIRED(2004, "登录失效"),
  ;

  private int code;
  private String msg;

  ResponseCodeSecurity(int code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  @Override
  public IResponseCodeEnum withMsg(String msg) {
    this.msg = msg;
    return this;
  }
}
