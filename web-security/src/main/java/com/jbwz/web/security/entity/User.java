package com.jbwz.web.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户
 *
 * @author yyh
 * @since 2021-07-15 22:48:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_s_user")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    @TableField("`password`")
    private String password;

    /**
     * 哪个公司的用户
     */
    @TableField("company_id")
    private Long companyId;

    /**
     * 部门Id
     */
    @TableField("department_id")
    private Long departmentId;

    /**
     * 1:超级管理员，2:普通管理员，3：一般用户
     */
    @TableField("admin_flag")
    private Integer adminFlag;

    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 姓名
     */
    @TableField("real_name")
    private String realName;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     */
    private Integer sex;
}
