package com.jbwz.web.security.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户
 *
 * @author yyh
 * @since 2021-07-15 22:48:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @Length(max = 16)
    @NotNull
    private String username;

    /**
     * 密码
     */
    @Length(max = 16, min = 3)
    private String password;

    /**
     * 密码
     */
    @Length(max = 16, min = 6)
    private String newPassword;
    /**
     * 哪个公司的用户
     */
    private Long companyId;

    /**
     * 部门Id
     */
    private Long departmentId;

    /**
     * 1:超级管理员，2:普通管理员，3：一般用户
     */
    private Integer adminFlag;

    /**
     * 昵称
     */
    @Length(max = 512)
    private String nickName;

    /**
     * 姓名
     */
    @Length(max = 512)
    private String realName;

    /**
     * 手机号
     */
    @Length(max = 512)
    private String phone;

    /**
     * 邮箱
     */
    @Length(max = 512)
    private String email;

    /**
     * 性别
     */
    private Integer sex;
    //角色id
    private List<Long> roleIds;

    //逗号隔开的角色名称
    @Length(max = 1024)
    private String roleNames;
}
