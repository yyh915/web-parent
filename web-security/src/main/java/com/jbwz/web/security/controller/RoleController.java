package com.jbwz.web.security.controller;


import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import com.jbwz.web.security.entity.Role;
import com.jbwz.web.security.entity.vo.RoleVO;
import com.jbwz.web.security.service.RoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

    @Autowired
    RoleService roleService;

    @GetMapping("/page-list")
    public ResponseJson pageList(BasePage page, RoleVO vo) {
        IBasePage result = roleService.pageList(page, vo);
        return success(result);
    }

    @PostMapping("/save")
    public ResponseJson save(@RequestBody @Validated RoleVO vo) {
        roleService.saveOrUpdateVO(vo);
        return success();
    }

    @GetMapping("/del/{id}")
    public ResponseJson del(@PathVariable("id") Long id) {
        roleService.removeById(id);
        return success();
    }

    @GetMapping("/detail/{id}")
    public ResponseJson detail(@PathVariable("id") Long id) {
        Role e = roleService.getById(id);
        return success(toVO(e));
    }

    private RoleVO toVO(Role e) {
        RoleVO vo = new RoleVO();
        BeanUtils.copyProperties(e, vo);
        return vo;
    }


}
