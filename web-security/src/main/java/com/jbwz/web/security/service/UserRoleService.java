package com.jbwz.web.security.service;

import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.security.entity.UserRole;

import java.util.List;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:44:43
 */
public interface UserRoleService extends BaseService<UserRole> {


    void delByUserId(Long id);

    /**
     * 根据用户id获取角色id
     *
     * @param id
     * @return
     */
    List<Long> getByUserId(Long id);

}
