package com.jbwz.web.security.controller;


import com.jbwz.web.common.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户角色 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@RestController
@RequestMapping("/user-role")
public class UserRoleController extends BaseController {

}
