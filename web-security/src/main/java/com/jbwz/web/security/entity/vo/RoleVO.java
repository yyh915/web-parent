package com.jbwz.web.security.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RoleVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    @Length(max = 512)
    private String name;

    /**
     * 描述
     */
    @Length(max = 512)
    private String description;

    /**
     * 公司关联
     */
    private Long companyId;
}
