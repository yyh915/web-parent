package com.jbwz.web.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 公司
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_s_company")
public class Company extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("`name`")
    private String name;

    @TableField("logo_url")
    private String logoUrl;

    /**
     * 创建人id
     */
    @TableField("created_id")
    private Long createdId;

    /**
     * 创建人名称
     */
    @TableField("created_name")
    private String createdName;


}
