package com.jbwz.web.security.service.biz;

import com.jbwz.web.security.entity.Company;
import com.jbwz.web.security.entity.User;
import com.jbwz.web.security.entity.vo.CompanyVO;
import com.jbwz.web.security.enums.AdminFlagEnum;
import com.jbwz.web.security.service.CompanyService;
import com.jbwz.web.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class CompanyUserBiz {
    @Autowired
    private UserService userService;
    @Autowired
    private CompanyService companyService;

    /**
     * 公司注册
     *
     * @param companyVO
     */
    @Transactional(rollbackFor = Exception.class)
    public void reg(CompanyVO companyVO) {
        Company company = companyService.saveValidate(companyVO);
        User user = new User();
        user.setUsername(companyVO.getUsername());
        user.setPassword(companyVO.getPassword());
        user.setAdminFlag(AdminFlagEnum.ADMIN.getCode());
        user.setCompanyId(company.getId());
        userService.saveByReg(user);
    }


}
