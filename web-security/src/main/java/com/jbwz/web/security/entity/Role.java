package com.jbwz.web.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author yyh
 * @since 2021-08-23 22:42:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_s_role")
public class Role extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 公司关联
     */
    private Long companyId;


}
