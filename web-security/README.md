# web-parent

#### 介绍

- 包含安全认证,用户创建，菜单创建，权限控制
- 可以作为一个单独的项目启动，做微服务
- 也可以引入到其他web项目做登录权限控制模块
- 表结构都是固定的