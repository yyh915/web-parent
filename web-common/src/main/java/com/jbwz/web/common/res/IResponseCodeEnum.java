package com.jbwz.web.common.res;

/**
 * 响应界面的code msg定义
 */
public interface IResponseCodeEnum {
    int getCode();

    String getMsg();

    /**
     * 修改为合适的返回界面的msg，
     * 共用一个code提供不同的msg
     * @param msg
     * @return
     */
    IResponseCodeEnum withMsg(String msg);

}
