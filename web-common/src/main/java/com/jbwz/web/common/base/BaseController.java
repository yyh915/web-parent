package com.jbwz.web.common.base;

import com.jbwz.web.common.res.IResponseCodeEnum;
import com.jbwz.web.common.res.ResponseJson;

public class BaseController {
    public ResponseJson success() {
        return ResponseJson.success();
    }

    public ResponseJson success(Object data) {
        return ResponseJson.success(data);
    }

    public ResponseJson fail(IResponseCodeEnum i) {
        return ResponseJson.fail(i);
    }

    public ResponseJson error() {
        return ResponseJson.error();
    }
}
