package com.jbwz.web.common.res;

import lombok.Data;

@Data
public class ResponseJson {
    private int code;
    private String msg;
    private Object data;

    public ResponseJson(IResponseCodeEnum i) {
        this.code = i.getCode();
        this.msg = i.getMsg();
    }

    public ResponseJson(IResponseCodeEnum i, Object data) {
        this(i);
        this.data = data;
    }

    public static ResponseJson success() {
        return new ResponseJson(ResponseCodeBase.success);
    }

    public static ResponseJson success(Object data) {
        return new ResponseJson(ResponseCodeBase.success, data);
    }

    public static ResponseJson fail(IResponseCodeEnum i) {
        return new ResponseJson(i);
    }

    public static ResponseJson error() {
        return new ResponseJson(ResponseCodeBase.error);
    }
}
