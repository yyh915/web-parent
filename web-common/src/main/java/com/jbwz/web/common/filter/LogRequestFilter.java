package com.jbwz.web.common.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class LogRequestFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest hsr = (HttpServletRequest) request;
        StringBuilder sb = new StringBuilder();
        Map<String, String[]> parameterMap = hsr.getParameterMap();
        sb.append("url:[").append(hsr.getServletPath()).append("]")
                .append(" method:[").append(hsr.getMethod()).append("]")
                .append(" params:[").append(parameterMap.keySet().stream().parallel().map(n ->
                        n + "=" + String.join(",", parameterMap.get(n))
                ).collect(Collectors.joining(" "))).append("]");
        log.info("请求>>>{}", sb);
        chain.doFilter(request, response);
    }
}
