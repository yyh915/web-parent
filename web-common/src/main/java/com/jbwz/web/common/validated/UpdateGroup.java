package com.jbwz.web.common.validated;

import javax.validation.groups.Default;

/**
 * 针对更新的时候使用校验
 */
public interface UpdateGroup extends Default {
}
