package com.jbwz.web.common.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public class BasePage<T> extends Page<T> implements IBasePage<T> {
    // 前端pro table使用这个字段
    private boolean success = true;
    private Long pageSize = 1L;

    public void setPageSize(Long pageSize) {
        super.setSize(pageSize);
        this.pageSize = pageSize;
    }

    public boolean isSuccess() {
        return success;
    }
}
