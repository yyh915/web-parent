package com.jbwz.web.common.res;

import lombok.Getter;

/**
 * base中的code都以
 * 10
 * 开头
 */
@Getter
public enum ResponseCodeBase implements IResponseCodeEnum {
    success(0, "成功"),
    error(1, "失败"),
    validFailed(1001, "请求参数校验失败"),
    dataExist(1002, "数据已存在"),
    ;

    private int code;
    private String msg;

    ResponseCodeBase(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public IResponseCodeEnum withMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
