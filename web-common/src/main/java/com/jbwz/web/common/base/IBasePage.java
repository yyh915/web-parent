package com.jbwz.web.common.base;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

public interface IBasePage<T> extends IPage<T> {
    /**
     * 转换entity to VO
     *
     * @param <R>    VO
     * @param mapper toVO
     * @return
     */
    default <R> IBasePage<R> convert(Function<? super T, ? extends R> mapper) {
        List collect = this.getRecords().stream().map(mapper).collect(toList());
        this.setRecords(collect);
        return (IBasePage<R>) this;
    }
}
