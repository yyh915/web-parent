package com.jbwz.web.common.session;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

@Slf4j
public class SecurityUserHolder {
    /**
     * 获取当前的登录人对象
     *
     * @return
     */
    public static SessionUser getCurrentUser() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            SessionUser principal = (SessionUser) authentication.getPrincipal();
            return principal;
        } catch (Exception e) {
            log.error("获取用户信息错误：", e);
        }
        return new SessionUser();
    }

    /**
     * 获取当前登录人的公司id
     *
     * @return
     */
    public static Long getCurrentCompanyId() {
        Long id = getCurrentUser().getCompanyId();
        return id;
    }

    /**
     * 获取当前登录人的id
     *
     * @return
     */
    public static Long getCurrentUserId() {
        Long id = getCurrentUser().getId();
        return id;
    }

    public static List<Long> getCurrentRoleIds() {
        List<Long> id = getCurrentUser().getRoleIds();
        return id;
    }
}
