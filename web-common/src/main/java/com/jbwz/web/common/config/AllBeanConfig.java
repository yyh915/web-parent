package com.jbwz.web.common.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Slf4j
@Configuration
public class AllBeanConfig {

    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";


    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return builder -> builder
                .timeZone("GMT+8")
                .serializerByType(Long.class, new ToStringSerializer())
                .serializers(new JsonSerializer<Void>() {
                    @Override
                    public Class<Void> handledType() {
                        return Void.class;
                    }

                    @Override
                    public void serialize(Void value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
                        gen.writeString("");
                    }
                })
                ;
    }
}
