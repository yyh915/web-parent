package com.jbwz.web.common.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jbwz.web.common.session.SecurityUserHolder;

public abstract class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T>
    implements BaseService<T> {
  protected QueryWrapper<T> queryWithCid() {
    Long currentCompanyId = SecurityUserHolder.getCurrentCompanyId();
    return Wrappers.<T>query().eq("company_id", currentCompanyId);
  }

  protected UpdateWrapper<T> updateWithCid() {
    Long currentCompanyId = SecurityUserHolder.getCurrentCompanyId();
    return Wrappers.<T>update().eq("company_id", currentCompanyId);
  }
  /**
   * 获取当前用户的公司id
   *
   * @return
   */
  protected Long getCurrentCid() {
    Long currentCompanyId = SecurityUserHolder.getCurrentCompanyId();
    return currentCompanyId;
  }
}
