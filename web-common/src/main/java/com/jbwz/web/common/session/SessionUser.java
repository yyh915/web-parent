package com.jbwz.web.common.session;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
public class SessionUser implements UserDetails {
    private Long id;
    private Long companyId;
    private List<Long> roleIds = new ArrayList<>();
    private String username;
    private String password;
    private Integer adminFlag;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;
    private Collection<? extends GrantedAuthority> authorities;
    private RequestMatcher requestMatcher;

}
