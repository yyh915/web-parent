package com.jbwz.web.common.util;

import org.springframework.beans.BeanUtils;

import java.util.Objects;

/** beanutil */
public class BeanUtilCommon {
  public static <R> R copy(Object source, Object dest) {
    if (Objects.isNull(source) || Objects.isNull(dest)) {
      throw new RuntimeException("beanCopy src des不能为null");
    }
    BeanUtils.copyProperties(source, dest);
    return (R) dest;
  }
}
