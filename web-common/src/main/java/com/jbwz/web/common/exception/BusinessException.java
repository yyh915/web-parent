package com.jbwz.web.common.exception;

import com.jbwz.web.common.res.IResponseCodeEnum;
import lombok.Getter;

@Getter
public class BusinessException extends RuntimeException {
    private IResponseCodeEnum resEnum;

    private BusinessException(IResponseCodeEnum enumI) {
        this.resEnum = enumI;
    }

    public static BusinessException resMsg(IResponseCodeEnum ienum) {
        return new BusinessException(ienum);
    }
}
