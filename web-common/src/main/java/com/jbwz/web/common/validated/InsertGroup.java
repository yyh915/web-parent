package com.jbwz.web.common.validated;

import javax.validation.groups.Default;

/**
 * 针对插入的时候使用校验
 */
public interface InsertGroup extends Default {
}
