package com.jbwz.web.generator;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.builder.GeneratorBuilder;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.keywords.MySqlKeyWordsHandler;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BaseEntity;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.generator.engine.CustomEngine;

public class GeneratorCodeApplication {
    public static void main(String[] args) {
        //
        //    "ss".substring(0,"s".lastIndexOf("."))
        String table_prefix = "t_sg_";
//        String[] tables = {"t_sg_customer", "t_sg_eatery",
//                "t_sg_goods", "t_sg_goods_category", "t_sg_goods_cr",
//                "t_sg_order", "t_sg_order_detail"
//        };
        String[] tables = {
                "t_sg_goods_"
        };
        //项目工程名称
        String moduleName = "cms";
        String packageName = "com.jbwz.web";
        DataSourceConfig dataSourceConfig =
                new DataSourceConfig.Builder(
                        "jdbc:mysql://106.13.140.154:3366/oa_db?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC",
                        "jbwz",
                        "123$jbwz")
                        .dbQuery(new MySqlQuery())
                        .typeConvert(new MySqlTypeConvert())
                        .keyWordsHandler(new MySqlKeyWordsHandler())
                        .build();
        TemplateConfig templateConfig = new TemplateConfig.Builder().build();
        PackageConfig packageConfig =
                new PackageConfig.Builder().parent(packageName).moduleName(moduleName).build();
        GlobalConfig globalConfig =
                GeneratorBuilder.globalConfigBuilder()
                        .fileOverride()
                        .outputDir("D:\\")
                        .author("yyh")
                        .dateType(DateType.TIME_PACK)
                        .commentDate("yyyy-MM-dd HH:mm:ss")
                        .build();
        StrategyConfig strategyConfig =
                new StrategyConfig.Builder()
                        .addTablePrefix(table_prefix)
                        .addInclude(tables)
                        // entity
                        .entityBuilder() // 实体配置构建者
                        .enableLombok() // 开启lombok模型
                        .superClass(BaseEntity.class)
                        .idType(IdType.ASSIGN_ID)
                        .naming(NamingStrategy.underline_to_camel) // 数据库表映射到实体的命名策略
                        .addSuperEntityColumns("create_time", "update_time", "id")
                        // controller
                        .controllerBuilder() // 控制器属性配置构建
                        .superClass(BaseController.class)
                        .enableHyphenStyle()
                        .enableRestStyle()
                        // service
                        .serviceBuilder()
                        .convertServiceFileName(name -> name + ConstVal.SERVICE)
                        .superServiceClass(BaseService.class)
                        .superServiceImplClass(BaseServiceImpl.class)
                        // mapper
                        .mapperBuilder()
                        .enableBaseResultMap()
                        .enableBaseColumnList()
                        .build();
        AutoGenerator autoGenerator =
                new AutoGenerator(dataSourceConfig)
                        .strategy(strategyConfig)
                        .packageInfo(packageConfig)
                        .template(templateConfig)
                        .global(globalConfig);
        autoGenerator.execute(new CustomEngine());
    }
}
