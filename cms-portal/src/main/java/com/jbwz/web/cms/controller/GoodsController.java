package com.jbwz.web.cms.controller;


import com.jbwz.web.cms.entity.Goods;
import com.jbwz.web.cms.entity.vo.GoodsCrVO;
import com.jbwz.web.cms.entity.vo.GoodsVO;
import com.jbwz.web.cms.service.GoodsService;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import com.jbwz.web.common.validated.InsertGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@RestController
@RequestMapping("/goods")
public class GoodsController extends BaseController {

    @Autowired
    GoodsService goodsService;

    @GetMapping("/page-list")
    public ResponseJson pageList(BasePage page, @Validated GoodsVO vo) {
        IBasePage result = goodsService.pageList(page, vo);
        return success(result);
    }


    @PostMapping("/save")
    public ResponseJson save(@Validated(InsertGroup.class) GoodsVO vo) {
        goodsService.saveOrUpdateVO(vo);
        return success();
    }

    @GetMapping("/del/{id}")
    public ResponseJson del(@PathVariable("id") Long id) {
        goodsService.deleteCascade(id);
        return success();
    }

    @GetMapping("/detail/{id}")
    public ResponseJson detail(@PathVariable("id") Long id) {
        Goods e = goodsService.getById(id);
        return success(goodsService.toVO(e));
    }

    @GetMapping("/status")
    public ResponseJson statusUpdate(GoodsVO vo) {
        Goods goods = new Goods();
        goods.setStatus(vo.getStatus());
        goods.setId(vo.getId());
        goodsService.updateById(goods);
        return success();
    }
}
