package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.GoodsLabel;
import com.jbwz.web.cms.entity.vo.GoodsLabelVO;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;

import java.util.List;

/**
 * 角色 服务类
 *
 * @author yyh
 * @since 2022-05-13 11:12:31
 */
public interface GoodsLabelService extends BaseService<GoodsLabel> {

  IBasePage pageList(IBasePage page, GoodsLabelVO vo);

  GoodsLabelVO toVO(GoodsLabel e);

  List<GoodsLabelVO> listByType(GoodsLabelVO vo);

  void saveOrUpdateVO(GoodsLabelVO vo);

  /**
   * 添加菜品时增加热点，方便下拉菜单展示常用的放第一个
   *
   * @param name
   */
  void addHotCount(String name);
}
