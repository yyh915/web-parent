package com.jbwz.web.cms.service.impl;

import com.jbwz.web.cms.common.SavedFileBS;
import com.jbwz.web.cms.config.properties.ProjectCMSProperties;
import com.jbwz.web.cms.entity.SavedFile;
import com.jbwz.web.cms.entity.vo.SavedFileVO;
import com.jbwz.web.cms.mapper.SavedFileMapper;
import com.jbwz.web.cms.service.SavedFileService;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.util.BeanUtilCommon;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.UUID;

/**
 * 角色 服务实现类
 *
 * @author yyh
 * @since 2022-04-10 17:20:39
 */
@Slf4j
@Service
public class SavedFileServiceImpl extends BaseServiceImpl<SavedFileMapper, SavedFile>
    implements SavedFileService {
  @Autowired ProjectCMSProperties cmsProperties;

  @Override
  public IBasePage pageList(IBasePage page, SavedFileVO vo) {
    //        return baseMapper.pageList(page, vo);
    return null;
  }

  @Override
  public void saveOrUpdateVO(SavedFileVO vo) {}

  @Override
  public void deleteById(Long id) {
    if (Objects.isNull(id)) {
      return;
    }
    SavedFile byId = getById(id);
    String filePath = byId.getFilePath();
    if (StringUtils.hasLength(filePath)) {
      File file = new File(filePath);
      file.delete();
    }
    removeById(id);
  }

  @Override
  public SavedFile saveFile(MultipartFile multipartFile) {
    if (Objects.isNull(multipartFile)) {
      return null;
    }
    String originalFilename = multipartFile.getOriginalFilename();
    if (!StringUtils.hasLength(originalFilename)) {
      originalFilename = "noname.jpg";
    }
    int length = originalFilename.length();
    if (length > 255) {
      originalFilename = originalFilename.substring(length - 250);
    }
    String ext = StringUtils.getFilenameExtension(originalFilename);
    if (ext.length() > 32) {
      ext = ext.substring(ext.length() - 33);
    }
    String fileName = StringUtils.getFilename(originalFilename);
    String uuid = StringUtils.replace(UUID.randomUUID().toString(), "-", "");
    String savedName = uuid + "." + ext;
    String savedPath =
        SavedFileBS.getSavedPath(
            SavedFileBS.EATERY_IMG_BS_TYPE, cmsProperties.getUploadPath(), savedName);
    SavedFile savedFile = new SavedFile();
    savedFile.setFileSize(multipartFile.getSize());
    savedFile.setOrgName(fileName);
    savedFile.setSavedName(savedName);
    savedFile.setFileExt(ext);
    savedFile.setFilePath(savedPath);
    try (InputStream inputStream = multipartFile.getInputStream();
        OutputStream outputStream = new FileOutputStream(savedPath)) {
      IOUtils.copy(inputStream, outputStream);
    } catch (Exception e) {
      log.error("保存文件错误：", e);
    }
    save(savedFile);
    return savedFile;
  }

  @Override
  public SavedFileVO toVO(SavedFile e) {
    SavedFileVO vo = BeanUtilCommon.copy(e, new SavedFileVO());
    return vo;
  }
}
