package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.Customer;
import com.jbwz.web.cms.entity.vo.CustomerVO;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface CustomerService extends BaseService<Customer> {

    IBasePage pageList(IBasePage page, CustomerVO vo);

    CustomerVO toVO(Customer e);

    void saveOrUpdateVO(CustomerVO vo);

}
