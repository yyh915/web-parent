package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_order_detail")
public class OrderDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 公司id
     */
    @TableField("company_id")
    private Long companyId;

    /**
     * 订单id
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 顾客id
     */
    @TableField("customer_id")
    private Long customerId;

    /**
     * 商品总金额
     */
    @TableField("total_amount")
    private BigDecimal totalAmount;

    /**
     * 商品数量
     */
    @TableField("goods_number")
    private Integer goodsNumber;

    /**
     * 订单状态
     */
    @TableField("order_status")
    private Integer orderStatus;

    /**
     * 商品id
     */
    @TableField("goods_id")
    private Long goodsId;

    /**
     * 当前商品价格
     */
    @TableField("gn_price")
    private BigDecimal gnPrice;

    /**
     * 商品原价
     */
    @TableField("go_price")
    private BigDecimal goPrice;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    private String goodsName;

    /**
     * 顾客手机号
     */
    private String phone;


}
