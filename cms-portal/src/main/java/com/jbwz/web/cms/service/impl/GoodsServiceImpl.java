package com.jbwz.web.cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jbwz.web.cms.common.viewenum.StatusEnum;
import com.jbwz.web.cms.entity.Goods;
import com.jbwz.web.cms.entity.SavedFile;
import com.jbwz.web.cms.entity.vo.GoodsCrVO;
import com.jbwz.web.cms.entity.vo.GoodsVO;
import com.jbwz.web.cms.mapper.GoodsMapper;
import com.jbwz.web.cms.service.GoodsCrService;
import com.jbwz.web.cms.service.GoodsLabelService;
import com.jbwz.web.cms.service.GoodsService;
import com.jbwz.web.cms.service.SavedFileService;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.exception.BusinessException;
import com.jbwz.web.common.res.ResponseCodeBase;
import com.jbwz.web.common.util.BeanUtilCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;

/**
 * 服务实现类
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Service
public class GoodsServiceImpl extends BaseServiceImpl<GoodsMapper, Goods> implements GoodsService {
  @Autowired GoodsCrService goodsCrService;
  @Autowired SavedFileService savedFileService;
  @Autowired GoodsLabelService goodsLabelService;

  @Override
  public IBasePage pageList(IBasePage page, GoodsVO vo) {
    QueryWrapper<Goods> queryChainWrapper =
        queryWithCid()
            .like(StringUtils.hasLength(vo.getName()), "name", vo.getName())
            .eq(Objects.nonNull(vo.getStatus()), "status", vo.getStatus())
            .orderByAsc("name");
    IBasePage<Goods> p = baseMapper.selectPage(page, queryChainWrapper);
    return p.convert(this::toVO);
  }

  @Override
  public void saveOrUpdateVO(GoodsVO vo) {
    QueryWrapper<Goods> query = queryWithCid().eq("name", vo.getName());
    Goods e = baseMapper.selectOne(query);
    if (Objects.nonNull(e)) {
      if (Objects.isNull(vo.getId()) || !e.getId().equals(vo.getId())) {
        throw BusinessException.resMsg(
            ResponseCodeBase.dataExist.withMsg(vo.getName() + "-菜品名称已存在"));
      }
    }
    if (Objects.isNull(vo.getStatus())) {
      // 设置默认值
      vo.setStatus(StatusEnum.ENABLE.code());
    }
    if (Objects.isNull(vo.getGnPrice())) {
      vo.setGnPrice(vo.getGoPrice());
    }
    // 增加label的热点count,如果是修改的时候没改变则不add
    if (Objects.isNull(e) || !Objects.equals(e.getFlavors(), vo.getFlavors())) {
      goodsLabelService.addHotCount(vo.getFlavors());
    }
    if (Objects.isNull(e) || !Objects.equals(e.getUnit(), vo.getUnit())) {
      goodsLabelService.addHotCount(vo.getUnit());
    }
    SavedFile savedFile = savedFileService.saveFile(vo.getFile());
    if (Objects.nonNull(savedFile)) {
      // 保存新的成功删除旧的图片
      if (Objects.nonNull(e)) {
        savedFileService.deleteById(e.getFileId());
      }
      vo.setFileId(savedFile.getId());
    }
    saveOrUpdate(toEntity(vo));
  }

  @Transactional(rollbackFor = Exception.class)
  @Override
  public void deleteCascade(Long id) {
    removeById(id);
    GoodsCrVO goodsCrVO = new GoodsCrVO();
    goodsCrVO.setGoodsId(id);
    goodsCrService.deleteByVO(goodsCrVO);
  }

  @Override
  public IBasePage listByCr(IBasePage page, GoodsCrVO vo) {
    IBasePage<GoodsVO> p = baseMapper.pageListByCr(page, vo);
    return p;
  }

  @Override
  public List listWithNoGcId(GoodsCrVO vo) {
    vo.setCompanyId(getCurrentCid());
    List<GoodsVO> p = baseMapper.listWithNoGcId(vo);
    return p;
  }

  @Override
  public GoodsVO toVO(Goods e) {
    GoodsVO vo = BeanUtilCommon.copy(e, new GoodsVO());
    return vo;
  }

  public Goods toEntity(GoodsVO vo) {
    Goods e = BeanUtilCommon.copy(vo, new Goods());
    e.setCompanyId(getCurrentCid());
    return e;
  }
}
