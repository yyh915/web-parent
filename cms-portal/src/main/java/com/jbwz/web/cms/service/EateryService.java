package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.Eatery;
import com.jbwz.web.cms.entity.vo.EateryVO;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface EateryService extends BaseService<Eatery> {

    IBasePage pageList(IBasePage page, EateryVO vo);

    EateryVO toVO(Eatery e);

    void saveOrUpdateVO(EateryVO vo);

}
