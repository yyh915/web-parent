package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.Order;
import com.jbwz.web.cms.entity.vo.OrderVO;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface OrderService extends BaseService<Order> {

    IBasePage pageList(IBasePage page, OrderVO vo);

    OrderVO toVO(Order e);

    void saveOrUpdateVO(OrderVO vo);

}
