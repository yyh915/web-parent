package com.jbwz.web.cms.mapper;

import com.jbwz.web.cms.entity.SavedFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author yyh
 * @since 2022-04-10 17:20:39
 */
public interface SavedFileMapper extends BaseMapper<SavedFile> {

}
