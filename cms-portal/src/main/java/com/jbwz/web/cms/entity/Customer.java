package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_customer")
public class Customer extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 小程序用户唯一id
     */
    @TableField("open_id")
    private String openId;

    @TableField("access_token")
    private String accessToken;

    @TableField("session_key")
    private String sessionKey;

    /**
     * 微信用户唯一id
     */
    @TableField("union_id")
    private String unionId;

    /**
     * 手机号
     */
    private String phone;


}
