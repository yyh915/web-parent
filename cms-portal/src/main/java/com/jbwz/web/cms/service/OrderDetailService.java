package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.OrderDetail;
import com.jbwz.web.cms.entity.vo.OrderDetailVO;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface OrderDetailService extends BaseService<OrderDetail> {

    IBasePage pageList(IBasePage page, OrderDetailVO vo);

    OrderDetailVO toVO(OrderDetail e);

    void saveOrUpdateVO(OrderDetailVO vo);

}
