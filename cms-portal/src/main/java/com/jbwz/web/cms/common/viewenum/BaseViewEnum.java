package com.jbwz.web.cms.common.viewenum;

import java.util.Optional;

public interface BaseViewEnum {
    default Optional<? extends BaseViewEnum> codeOf(Integer code) {
        return Optional.empty();
    }
    int code();

    String msg();
}
