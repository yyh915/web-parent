package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_goods_cr")
public class GoodsCr extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 商品分类id
     */
    @TableField("category_id")
    private Long categoryId;

    /**
     * 排序
     */
    private Integer sorted;

    /**
     * 商品id
     */
    @TableField("goods_id")
    private Long goodsId;

    /**
     * 1:启用，0：禁用
     */
    @TableField("`status`")
    private Boolean status;


}
