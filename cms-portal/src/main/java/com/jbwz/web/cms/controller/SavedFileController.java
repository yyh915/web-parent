package com.jbwz.web.cms.controller;


import com.jbwz.web.cms.entity.SavedFile;
import com.jbwz.web.cms.entity.vo.SavedFileVO;
import com.jbwz.web.cms.service.SavedFileService;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2022-04-10 17:20:39
 */
@RestController
@RequestMapping("/saved-file")
public class SavedFileController extends BaseController {

    @Autowired
    SavedFileService savedFileService;

    @GetMapping("/page-list")
    public ResponseJson pageList(BasePage page, @Validated SavedFileVO vo) {
        IBasePage result = savedFileService.pageList(page, vo);
        return success(result);
    }

    @PostMapping("/save")
    public ResponseJson save(@RequestBody @Validated SavedFileVO vo) {
        savedFileService.saveOrUpdateVO(vo);
        return success();
    }

    @GetMapping("/del/{id}")
    public ResponseJson del(@PathVariable("id") Long id) {
        return success();
    }

    @GetMapping("/detail/{id}")
    public ResponseJson detail(@PathVariable("id") Long id) {
        SavedFile e = savedFileService.getById(id);
        return success(savedFileService.toVO(e));
    }

}
