package com.jbwz.web.cms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = "com.jbwz.web")
// @MapperScan("com.jbwz.web.*.mapper")
public class CMSApplication {

  public static void main(String[] args) {
    SpringApplication springApplication = new SpringApplication(CMSApplication.class);
    ConfigurableApplicationContext run = springApplication.run(args);
  }
}
