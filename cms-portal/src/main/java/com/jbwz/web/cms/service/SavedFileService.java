package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.SavedFile;
import com.jbwz.web.common.base.BaseService;

import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.cms.entity.vo.SavedFileVO;
import org.springframework.web.multipart.MultipartFile;

/**
 * 角色 服务类
 *
 * @author yyh
 * @since 2022-04-10 17:20:39
 */
public interface SavedFileService extends BaseService<SavedFile> {

  IBasePage pageList(IBasePage page, SavedFileVO vo);

  void deleteById(Long id);

  /**
   * 保存图片,不管是修改(保存文件时会根据文件名字是否是UUID判断图片是否改变，如果不改变返回null) 还是新增都重新保存一次图片
   *
   * @param multipartFile
   * @return
   */
  SavedFile saveFile(MultipartFile multipartFile);

  SavedFileVO toVO(SavedFile e);

  void saveOrUpdateVO(SavedFileVO vo);
}
