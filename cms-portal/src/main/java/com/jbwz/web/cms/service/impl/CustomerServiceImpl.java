package com.jbwz.web.cms.service.impl;

import com.jbwz.web.cms.entity.Customer;
import com.jbwz.web.cms.entity.vo.CustomerVO;
import com.jbwz.web.cms.mapper.CustomerMapper;
import com.jbwz.web.cms.service.CustomerService;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.util.BeanUtilCommon;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Service
public class CustomerServiceImpl extends BaseServiceImpl<CustomerMapper, Customer> implements CustomerService {

    @Override
    public IBasePage pageList(IBasePage page, CustomerVO vo) {
//        return baseMapper.pageList(page, vo);
        return null;
    }

    @Override
    public void saveOrUpdateVO(CustomerVO vo) {

    }

    @Override
    public CustomerVO toVO(Customer e) {
        CustomerVO vo = BeanUtilCommon.copy(e, new CustomerVO());
        return vo;
    }

    public Customer toEntity(CustomerVO vo) {
        Customer e = BeanUtilCommon.copy(vo, new Customer());
        return e;
    }
}
