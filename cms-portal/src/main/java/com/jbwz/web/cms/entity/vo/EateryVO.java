package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EateryVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @Length(max = 512)
    private String name;

    /**
     * 1:删除
     */
    private Boolean delFlag;

    /**
     * 描述
     */
    @Length(max = 512)
    private String description;

    /**
     * 公司id
     */
    private Long companyId;
}
