package com.jbwz.web.cms.common;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 保存文件业务相关
 */
@Slf4j
public class SavedFileBS {
    //菜品图片
    public static final String EATERY_IMG_BS_TYPE = "eatery-img";

    public static String getSavedPath(String bsType, String basePath, String fileName) {
        String yyyymmdd = LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYYMMdd"));
        String path = String.join("/", yyyymmdd, bsType, fileName);
        String p;
        if (StringUtils.endsWithIgnoreCase(basePath, "/")) {
            p = basePath + path;
        } else {
            p = basePath + "/" + path;
        }
        File file = new File(p);
        try {
            FileUtils.forceMkdirParent(file);
        } catch (IOException e) {
            log.error("创建文件夹错误:", e);
        }
        return p;
    }

}
