package com.jbwz.web.cms.enums;

import com.jbwz.web.cms.common.viewenum.BaseViewEnum;

/**
 * 删除标记
 */
public enum DelFlagEnum implements BaseViewEnum {
    no(0, "未删除"), yes(1, "已删除");
    private int code;
    private String msg;

    DelFlagEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String msg() {
        return msg;
    }
}
