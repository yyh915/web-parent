package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * 角色
 *
 * @author yyh
 * @since 2022-05-13 11:12:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsLabelVO extends BaseVO {

  private static final long serialVersionUID = 1L;

  /** 名称 */
  @Length(max = 512)
  private String name;

  /** 1:单位，2:口味，3:下单备注等类型 */
  @NotNull private Integer type;

  /** 公司id */
  private Long companyId;

  /** 排序热点 添加菜品时增加热点，方便下拉菜单展示常用的放第一个 */
  private Integer hotCount;
}
