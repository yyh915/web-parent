/**
 * 主要用于接收界面的字典数据,及保存到收据库的状态值对应
 * 例如：根据界面提交的1,0对应启用，禁用状态
 */
package com.jbwz.web.cms.common.viewenum;