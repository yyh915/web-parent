package com.jbwz.web.cms.config.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@Data
@ConfigurationProperties(prefix = "project")
public class ProjectCMSProperties {
    private LoginProperties login;
    private String uploadPath = "/opt/upload";

    @Data
    public static class LoginProperties {
        private int maxErrorTimes = 3;
        private Duration waitTime = Duration.ofMinutes(3L);
    }
}

