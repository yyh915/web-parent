package com.jbwz.web.cms.controller;


import com.jbwz.web.cms.entity.Customer;
import com.jbwz.web.cms.entity.vo.CustomerVO;
import com.jbwz.web.cms.service.CustomerService;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@RestController
@RequestMapping("/customer")
public class CustomerController extends BaseController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/page-list")
    public ResponseJson pageList(BasePage page, @Validated CustomerVO vo) {
        IBasePage result = customerService.pageList(page, vo);
        return success(result);
    }

    @PostMapping("/save")
    public ResponseJson save(@RequestBody @Validated CustomerVO vo) {
        customerService.saveOrUpdateVO(vo);
        return success();
    }

    @GetMapping("/del/{id}")
    public ResponseJson del(@PathVariable("id") Long id) {
        return success();
    }

    @GetMapping("/detail/{id}")
    public ResponseJson detail(@PathVariable("id") Long id) {
        Customer e = customerService.getById(id);
        return success(customerService.toVO(e));
    }

}
