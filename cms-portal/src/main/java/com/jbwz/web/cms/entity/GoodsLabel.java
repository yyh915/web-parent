package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色
 *
 * @author yyh
 * @since 2022-05-13 11:12:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_goods_label")
public class GoodsLabel extends BaseEntity {

  private static final long serialVersionUID = 1L;

  /** 名称 */
  private String name;

  /** 1:单位，2:口味，3:下单备注等类型 */
  private Integer type;

  /** 公司id */
  private Long companyId;

  /** 排序热点
   * 添加菜品时增加热点，方便下拉菜单展示常用的放第一个
   * */
  private Integer hotCount;
}
