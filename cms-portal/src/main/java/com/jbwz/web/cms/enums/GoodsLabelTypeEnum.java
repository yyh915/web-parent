package com.jbwz.web.cms.enums;

import com.jbwz.web.cms.common.viewenum.BaseViewEnum;

/** 删除标记 */
public enum GoodsLabelTypeEnum implements BaseViewEnum {
  unit(1, "单位"),
  taste(2, "口味"),
  flavor(3, "用户下单备注"),
  ;
  private int code;
  private String msg;

  GoodsLabelTypeEnum(Integer code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  @Override
  public int code() {
    return code;
  }

  @Override
  public String msg() {
    return msg;
  }
}
