package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.Goods;
import com.jbwz.web.cms.entity.vo.GoodsCrVO;
import com.jbwz.web.cms.entity.vo.GoodsVO;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface GoodsService extends BaseService<Goods> {

    IBasePage pageList(IBasePage page, GoodsVO vo);

    void deleteCascade(Long id);

    void saveOrUpdateVO(GoodsVO vo);

    GoodsVO toVO(Goods e);

    /**
     * 根据中间表查询菜品
     *
     * @param page
     * @param vo
     * @return
     */
    IBasePage listByCr(IBasePage page, GoodsCrVO vo);

    /**
     * 查询当前分类未添加过的菜品
     */
    List listWithNoGcId(GoodsCrVO vo);
}
