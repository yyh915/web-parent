package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_eatery")
public class Eatery extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 1:删除
     */
    @TableField("del_flag")
    private Boolean delFlag;

    /**
     * 描述
     */
    private String description;

    /**
     * 公司id
     */
    @TableField("company_id")
    private Long companyId;


}
