package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.GoodsCategory;
import com.jbwz.web.cms.entity.vo.GoodsCategoryVO;
import com.jbwz.web.common.base.BaseService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface GoodsCategoryService extends BaseService<GoodsCategory> {


    GoodsCategoryVO toVO(GoodsCategory e);

    List<GoodsCategory> listFind(GoodsCategoryVO vo);

    void updateSorted(List<GoodsCategoryVO> vos);

    void saveOrUpdateVO(GoodsCategoryVO vo);

}
