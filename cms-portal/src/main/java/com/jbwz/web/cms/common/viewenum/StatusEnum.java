package com.jbwz.web.cms.common.viewenum;

import java.util.Arrays;
import java.util.Optional;

/**
 * 状态相关的数字含义描述
 */
public enum StatusEnum implements BaseViewEnum {
    ENABLE(1, "启用"),
    DISABLE(0, "禁用");
    private int code;
    private String msg;

    StatusEnum(int i, String msg) {
        this.code = i;
        this.msg = msg;
    }

    public Optional<StatusEnum> codeOf(Integer code) {
        Optional<StatusEnum> any = Arrays.stream(StatusEnum.values()).filter(s -> s.code == code).findAny();
        return any;
    }

    public int code() {
        return code;
    }

    public String msg() {
        return msg;
    }
}
