package com.jbwz.web.cms.service;

import com.jbwz.web.cms.entity.GoodsCr;
import com.jbwz.web.cms.entity.vo.GoodsCrVO;
import com.jbwz.web.common.base.BaseService;
import com.jbwz.web.common.base.IBasePage;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface GoodsCrService extends BaseService<GoodsCr> {

    IBasePage pageList(IBasePage page, GoodsCrVO vo);

    GoodsCrVO toVO(GoodsCr e);

    void save(GoodsCrVO vo);

    void updateSorted(GoodsCrVO vo);

    List<GoodsCr> findByCategoryId(Long id);

    void deleteByVO(GoodsCrVO vo);
}
