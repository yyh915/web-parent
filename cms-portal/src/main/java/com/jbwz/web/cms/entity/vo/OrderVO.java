package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrderVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 支付时间
     */
    private LocalDateTime payTime;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 订单号
     */
    @Length(max = 512)
    private String orderNo;

    /**
     * 顾客id
     */
    private Long customerId;

    /**
     * 0：未支付，11：支付中，12：支付成功,13：支付失败，4：取消支付，51：退款中，52：退款失败,53：退款款成功
     */
    @Size(max = 99)
    private Integer orderStatus;

    /**
     * 排队号
     */
    @Length(max = 512)
    private String waiteNo;

    /**
     * 支付类型，1：小程序支付
     */
    private Integer payType;

    /**
     * 支付后第三方支付结果id
     */
    @Length(max = 512)
    private String payId;

    /**
     * 顾客手机号
     */
    @Length(max = 512)
    private String customerPhone;

    /**
     * 订单备注
     */
    @Length(max = 512)
    private String orderRemark;

    /**
     * 支付金额，支付后回调，和total_amount对比，如果不一致，订单就有问题
     */
    private BigDecimal payAmount;

    /**
     * 订单总额
     */
    private BigDecimal totalAmount;

    /**
     * 商品数量
     */
    private Integer goodsNumber;

    /**
     * 顾客手机号
     */
    @Length(max = 512)
    private String phone;
}
