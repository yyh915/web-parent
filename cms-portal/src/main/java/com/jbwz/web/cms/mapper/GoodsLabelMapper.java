package com.jbwz.web.cms.mapper;

import com.jbwz.web.cms.entity.GoodsLabel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author yyh
 * @since 2022-05-13 11:12:31
 */
public interface GoodsLabelMapper extends BaseMapper<GoodsLabel> {

}
