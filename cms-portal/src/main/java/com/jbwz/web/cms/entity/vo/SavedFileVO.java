package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * <p>
 * 保存文件
 * </p>
 *
 * @author yyh
 * @since 2022-04-10 17:20:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SavedFileVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 原始文件名
     */
    @Length(max = 512)
    private String orgName;

    /**
     * 保存后文件名
     */
    @Length(max = 512)
    private String savedName;

    /**
     * 文件保存路径
     */
    @Length(max = 512)
    private String filePath;

    /**
     * 文件大小
     */
    private Long fileSize;

    /**
     * 文件扩展名
     */
    @Length(max = 512)
    private String fileExt;

    /**
     * 业务类型
     */
    @Length(max = 512)
    private String bsType;
}
