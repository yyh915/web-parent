package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import com.jbwz.web.common.validated.InsertGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsVO extends BaseVO {

    private static final long serialVersionUID = 1L;
    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 库存剩余，根据每日展示
     */
    private Integer surplusStock;

    /**
     * 商品库存，-1无限,每日重置
     */
    private Integer dayStock;


    /**
     * 当前商品价格
     */
    private BigDecimal gnPrice;

    /**
     * 商品原价
     */
    private BigDecimal goPrice;

    /**
     * 商品名称
     */
    @Length(max = 512, groups = InsertGroup.class)
    @NotNull(groups = InsertGroup.class)
    private String name;

    /**
     * 商品描述
     */
    @Length(max = 512)
    private String description;

    /**
     * 1:上架，0：下架
     */
    @Max(9)
    private Integer status;

    /**
     * 1:已删除 ,0:未删除
     */
    private Boolean delFlag;

    /**
     * 销量
     */
    private Integer sales;

    /**
     * 口味标签
     */
    @Length(max = 512)
    private String flavors;

    /**
     * 单位
     */
    @Length(max = 512)
    private String unit;

    /**
     * 图片
     */
    @Length(max = 512)
    private String picture;

    private Long fileId;

    //关联表查询字段
    //关联表主键
    private Long gcrId;
    private Long goodsId;
    private Integer sorted;
    /**
     * 商品i类别id
     */
    private Long categoryId;

    MultipartFile file;
}
