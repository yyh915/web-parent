package com.jbwz.web.cms.common;

import com.jbwz.web.common.res.IResponseCodeEnum;
import lombok.Getter;

/**
 * code都以30开头
 */
@Getter
public enum ResponseCodeCMS implements IResponseCodeEnum {
    HAS_RELATION_SHIP(3010, "有关联数据"),
    FILE_DOWNLOAD_ERR(3210, "下载文件错误"),
    ;

    private int code;
    private String msg;

    ResponseCodeCMS(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public IResponseCodeEnum withMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
