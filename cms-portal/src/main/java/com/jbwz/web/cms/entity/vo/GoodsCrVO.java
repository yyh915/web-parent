package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsCrVO extends BaseVO {

  private static final long serialVersionUID = 1L;

  /** 商品分类id */
  private Long categoryId;

  /** 排序 */
  private Integer sorted;

  /** 商品id */
  private Long goodsId;

  /** 1:启用，0：禁用 */
  private Boolean status;
  // 查询菜品名称
  private String name;
  /** 商品分类id */
  private Long companyId;
  // 根据分类id排列菜品顺序使用
  private List<GoodsCrVO> sortList = new ArrayList<>();
  // 添加关联使用
  private List<Long> goodsIdList = new ArrayList<>();
}
