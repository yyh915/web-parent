package com.jbwz.web.cms.service.impl;

import com.jbwz.web.cms.entity.Order;
import com.jbwz.web.cms.entity.vo.OrderVO;
import com.jbwz.web.cms.mapper.OrderMapper;
import com.jbwz.web.cms.service.OrderService;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.util.BeanUtilCommon;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Service
public class OrderServiceImpl extends BaseServiceImpl<OrderMapper, Order> implements OrderService {

    @Override
    public IBasePage pageList(IBasePage page, OrderVO vo) {
//        return baseMapper.pageList(page, vo);
        return null;
    }

    @Override
    public void saveOrUpdateVO(OrderVO vo) {

    }

    @Override
    public OrderVO toVO(Order e) {
        OrderVO vo = BeanUtilCommon.copy(e, new OrderVO());
        return vo;
    }

    public Order toEntity(OrderVO vo) {
        Order e = BeanUtilCommon.copy(vo, new Order());
        return e;
    }
}
