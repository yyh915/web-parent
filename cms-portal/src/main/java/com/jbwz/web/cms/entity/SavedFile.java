package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author yyh
 * @since 2022-04-10 17:20:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_saved_file")
public class SavedFile extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 原始文件名
     */
    @TableField("org_name")
    private String orgName;

    /**
     * 保存后文件名
     */
    @TableField("saved_name")
    private String savedName;

    /**
     * 文件保存路径
     */
    @TableField("file_path")
    private String filePath;

    /**
     * 文件大小
     */
    @TableField("file_size")
    private Long fileSize;

    /**
     * 文件扩展名
     */
    @TableField("file_ext")
    private String fileExt;

    /**
     * 业务类型
     */
    @TableField("bs_type")
    private String bsType;


}
