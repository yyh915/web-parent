package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import com.jbwz.web.common.validated.InsertGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsCategoryVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @Length(max = 512, groups = InsertGroup.class)
    @NotNull(groups = InsertGroup.class)
    private String name;

    /**
     * 排序
     */
    private Integer sorted;

    /**
     * 1：启用，0：禁用
     */
    private Integer status;

    /**
     * 1:删除
     */
    private Integer delFlag;

    /**
     * 描述
     */
    @Length(max = 512)
    private String description;

    /**
     * 公司id
     */
    private Long companyId;
}
