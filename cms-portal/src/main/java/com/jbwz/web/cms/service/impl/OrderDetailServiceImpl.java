package com.jbwz.web.cms.service.impl;

import com.jbwz.web.cms.entity.OrderDetail;
import com.jbwz.web.cms.entity.vo.OrderDetailVO;
import com.jbwz.web.cms.mapper.OrderDetailMapper;
import com.jbwz.web.cms.service.OrderDetailService;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.util.BeanUtilCommon;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Service
public class OrderDetailServiceImpl extends BaseServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {

    @Override
    public IBasePage pageList(IBasePage page, OrderDetailVO vo) {
//        return baseMapper.pageList(page, vo);
        return null;
    }

    @Override
    public void saveOrUpdateVO(OrderDetailVO vo) {

    }

    @Override
    public OrderDetailVO toVO(OrderDetail e) {
        OrderDetailVO vo = BeanUtilCommon.copy(e, new OrderDetailVO());
        return vo;
    }

    public OrderDetail toEntity(OrderDetailVO vo) {
        OrderDetail e = BeanUtilCommon.copy(vo, new OrderDetail());
        return e;
    }
}
