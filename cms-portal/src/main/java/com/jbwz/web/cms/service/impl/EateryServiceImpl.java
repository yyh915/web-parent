package com.jbwz.web.cms.service.impl;

import com.jbwz.web.cms.entity.Eatery;
import com.jbwz.web.cms.entity.vo.EateryVO;
import com.jbwz.web.cms.mapper.EateryMapper;
import com.jbwz.web.cms.service.EateryService;
import com.jbwz.web.common.base.BaseServiceImpl;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.util.BeanUtilCommon;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * 服务实现类
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Service
public class EateryServiceImpl extends BaseServiceImpl<EateryMapper, Eatery>
    implements EateryService {

  @Override
  public IBasePage pageList(IBasePage page, EateryVO vo) {
    //        return baseMapper.pageList(page, vo);
    return null;
  }

  @Override
  public void saveOrUpdateVO(EateryVO vo) {}

  @Override
  public EateryVO toVO(Eatery e) {
    EateryVO vo = BeanUtilCommon.copy(e, new EateryVO());
    return vo;
  }

  public Eatery toEntity(EateryVO vo) {
    Eatery e = BeanUtilCommon.copy(vo, new Eatery());
    return e;
  }
}
