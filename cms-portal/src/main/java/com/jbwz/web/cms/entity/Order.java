package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_order")
public class Order extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 支付时间
     */
    @TableField("pay_time")
    private LocalDateTime payTime;

    /**
     * 公司id
     */
    @TableField("company_id")
    private Long companyId;

    /**
     * 订单号
     */
    @TableField("order_no")
    private String orderNo;

    /**
     * 顾客id
     */
    @TableField("customer_id")
    private Long customerId;

    /**
     * 0：未支付，11：支付中，12：支付成功,13：支付失败，4：取消支付，51：退款中，52：退款失败,53：退款款成功
     */
    @TableField("order_status")
    private Integer orderStatus;

    /**
     * 排队号
     */
    @TableField("waite_no")
    private String waiteNo;

    /**
     * 支付类型，1：小程序支付
     */
    @TableField("pay_type")
    private Integer payType;

    /**
     * 支付后第三方支付结果id
     */
    @TableField("pay_id")
    private String payId;

    /**
     * 顾客手机号
     */
    @TableField("customer_phone")
    private String customerPhone;

    /**
     * 订单备注
     */
    @TableField("order_remark")
    private String orderRemark;

    /**
     * 支付金额，支付后回调，和total_amount对比，如果不一致，订单就有问题
     */
    @TableField("pay_amount")
    private BigDecimal payAmount;

    /**
     * 订单总额
     */
    @TableField("total_amount")
    private BigDecimal totalAmount;

    /**
     * 商品数量
     */
    @TableField("goods_number")
    private Integer goodsNumber;

    /**
     * 顾客手机号
     */
    private String phone;


}
