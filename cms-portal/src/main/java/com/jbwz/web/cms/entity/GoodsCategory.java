package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_goods_category")
public class GoodsCategory extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 排序
     */
    private Integer sorted;

    /**
     * 1：启用，0：禁用
     */
    @TableField("`status`")
    private Integer status;

    /**
     * 1:删除
     */
    @TableField("del_flag")
    private Integer delFlag;

    /**
     * 描述
     */
    private String description;

    /**
     * 公司id
     */
    @TableField("company_id")
    private Long companyId;


}
