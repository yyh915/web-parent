package com.jbwz.web.cms.controller;

import com.jbwz.web.cms.entity.GoodsLabel;
import com.jbwz.web.cms.entity.vo.GoodsLabelVO;
import com.jbwz.web.cms.service.GoodsLabelService;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.res.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
/**
 * 菜品标签 前端控制器
 *
 * @author yyh
 * @since 2022-05-13 11:12:31
 */
@RestController
@RequestMapping("/goods-label")
public class GoodsLabelController extends BaseController {

  @Autowired GoodsLabelService goodsLabelService;

  @GetMapping("/list")
  public ResponseJson pageList(@Validated GoodsLabelVO vo) {
    return success(goodsLabelService.listByType(vo));
  }

  @PostMapping("/save")
  public ResponseJson save(@RequestBody @Validated GoodsLabelVO vo) {
    goodsLabelService.saveOrUpdateVO(vo);
    return success();
  }

  @GetMapping("/del/{id}")
  public ResponseJson del(@PathVariable("id") Long id) {
    goodsLabelService.removeById(id);
    return success();
  }

  @GetMapping("/detail/{id}")
  public ResponseJson detail(@PathVariable("id") Long id) {
    GoodsLabel e = goodsLabelService.getById(id);
    return success(goodsLabelService.toVO(e));
  }
}
