package com.jbwz.web.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jbwz.web.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sg_goods")
public class Goods extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 公司id
     */
    @TableField("company_id")
    private Long companyId;

    /**
     * 图片文件id
     */
    @TableField("file_id")
    private Long fileId;
    /**
     * 库存剩余，根据每日展示
     */
    @TableField("surplus_stock")
    private Integer surplusStock;

    /**
     * 商品库存，-1无限,每日重置
     */
    @TableField("day_stock")
    private Integer dayStock;


    /**
     * 当前商品价格
     */
    @TableField("gn_price")
    private BigDecimal gnPrice;

    /**
     * 商品原价
     */
    @TableField("go_price")
    private BigDecimal goPrice;

    /**
     * 商品名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 商品描述
     */
    private String description;

    /**
     * 1:上架，0：下架
     */
    @TableField("`status`")
    private Integer status;

    /**
     * 1:已删除 ,0:未删除
     */
    @TableField("del_flag")
    private Boolean delFlag;

    /**
     * 销量
     */
    private Integer sales;

    /**
     * 口味标签
     */
    private String flavors;

    /**
     * 单位
     */
    private String unit;

    /**
     * 图片
     */
    private String picture;


}
