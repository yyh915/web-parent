package com.jbwz.web.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jbwz.web.cms.entity.Goods;
import com.jbwz.web.cms.entity.vo.GoodsCrVO;
import com.jbwz.web.cms.entity.vo.GoodsVO;
import com.jbwz.web.common.base.IBasePage;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    /**
     * 根据中间表插叙菜品
     * @param page
     * @param vo
     * @return
     */
    IBasePage<GoodsVO> pageListByCr(IBasePage page, GoodsCrVO vo);

    List<GoodsVO> listWithNoGcId(GoodsCrVO vo);
}

