package com.jbwz.web.cms.controller;

import com.jbwz.web.cms.entity.vo.GoodsCrVO;
import com.jbwz.web.cms.service.GoodsCrService;
import com.jbwz.web.cms.service.GoodsService;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 前端控制器
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@RestController
@RequestMapping("/goods-home")
public class GoodsHomeController extends BaseController {

  @Autowired GoodsCrService goodsCrService;

  @Autowired GoodsService goodsService;

  @GetMapping("/page-list")
  public ResponseJson listByCr(BasePage page, @Validated GoodsCrVO vo) {
    IBasePage list = goodsService.listByCr(page, vo);
    return success(list);
  }

  // 查询当前分类未添加过的菜品
  @GetMapping("/list-nogcrid")
  public ResponseJson listWithNoGcId(@Validated GoodsCrVO vo) {
    List list = goodsService.listWithNoGcId(vo);
    return success(list);
  }

  @PostMapping("/save")
  public ResponseJson save(@RequestBody @Validated GoodsCrVO vo) {
    vo.getGoodsIdList()
        .forEach(
            gid -> {
              GoodsCrVO goodsCrVO = new GoodsCrVO();
              goodsCrVO.setCategoryId(vo.getCategoryId());
              goodsCrVO.setGoodsId(gid);
              goodsCrService.save(goodsCrVO);
            });
    return success();
  }

  @PostMapping("/sort-up")
  public ResponseJson sortUpdate(@RequestBody @Validated GoodsCrVO vo) {
    goodsCrService.updateSorted(vo);
    return success();
  }

  @GetMapping("/del")
  public ResponseJson del(GoodsCrVO vo) {
    goodsCrService.removeById(vo.getId());
    return success();
  }
}
