package com.jbwz.web.cms.controller;


import com.jbwz.web.cms.entity.Order;
import com.jbwz.web.cms.entity.vo.OrderVO;
import com.jbwz.web.cms.service.OrderService;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@RestController
@RequestMapping("/order")
public class OrderController extends BaseController {

    @Autowired
    OrderService orderService;

    @GetMapping("/page-list")
    public ResponseJson pageList(BasePage page, @Validated OrderVO vo) {
        IBasePage result = orderService.pageList(page, vo);
        return success(result);
    }

    @PostMapping("/save")
    public ResponseJson save(@RequestBody @Validated OrderVO vo) {
        orderService.saveOrUpdateVO(vo);
        return success();
    }

    @GetMapping("/del/{id}")
    public ResponseJson del(@PathVariable("id") Long id) {
        return success();
    }

    @GetMapping("/detail/{id}")
    public ResponseJson detail(@PathVariable("id") Long id) {
        Order e = orderService.getById(id);
        return success(orderService.toVO(e));
    }

}
