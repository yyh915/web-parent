package com.jbwz.web.cms.controller;


import com.jbwz.web.cms.entity.OrderDetail;
import com.jbwz.web.cms.entity.vo.OrderDetailVO;
import com.jbwz.web.cms.service.OrderDetailService;
import com.jbwz.web.common.base.BaseController;
import com.jbwz.web.common.base.BasePage;
import com.jbwz.web.common.base.IBasePage;
import com.jbwz.web.common.res.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@RestController
@RequestMapping("/order-detail")
public class OrderDetailController extends BaseController {

    @Autowired
    OrderDetailService orderDetailService;

    @GetMapping("/page-list")
    public ResponseJson pageList(BasePage page, @Validated OrderDetailVO vo) {
        IBasePage result = orderDetailService.pageList(page, vo);
        return success(result);
    }

    @PostMapping("/save")
    public ResponseJson save(@RequestBody @Validated OrderDetailVO vo) {
        orderDetailService.saveOrUpdateVO(vo);
        return success();
    }

    @GetMapping("/del/{id}")
    public ResponseJson del(@PathVariable("id") Long id) {
        return success();
    }

    @GetMapping("/detail/{id}")
    public ResponseJson detail(@PathVariable("id") Long id) {
        OrderDetail e = orderDetailService.getById(id);
        return success(orderDetailService.toVO(e));
    }

}
