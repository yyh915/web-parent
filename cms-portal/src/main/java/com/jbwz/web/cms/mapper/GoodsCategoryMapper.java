package com.jbwz.web.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jbwz.web.cms.entity.GoodsCategory;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
public interface GoodsCategoryMapper extends BaseMapper<GoodsCategory> {

}
