package com.jbwz.web.cms.entity.vo;

import com.jbwz.web.common.base.BaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author yyh
 * @since 2022-02-03 09:32:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrderDetailVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 顾客id
     */
    private Long customerId;

    /**
     * 商品总金额
     */
    private BigDecimal totalAmount;

    /**
     * 商品数量
     */
    private Integer goodsNumber;

    /**
     * 订单状态
     */
    @Size(max = 99)
    private Integer orderStatus;

    /**
     * 商品id
     */
    private Long goodsId;

    /**
     * 当前商品价格
     */
    private BigDecimal gnPrice;

    /**
     * 商品原价
     */
    private BigDecimal goPrice;

    /**
     * 商品名称
     */
    @Length(max = 512)
    private String goodsName;

    /**
     * 顾客手机号
     */
    @Length(max = 512)
    private String phone;
}
