### 脚本说明

cms-portal 目录主要是上传到云服务器上开发测试使用

```bash
cms-portal/bin 目录下文件说明
只执行一次的初始化脚本
1. InstallDockerNet.sh  创建docker网络,必须第一个运行
2. InitSqlData.sh  初始化mysql数据库 

正常启动的脚本顺序
1. start-mysql.sh 启动msql
2. update-cms.sh  启动cms-server,并更新html代码,这个脚本会调用 start-cms.sh
3. start-nginx.sh  启动nginx

cms-portal/sql 这个目录是存放初始化数据库和表的sql,每次数据库表结构修改都要同步修改oa_db.sql,可以使用navicat导出oa_db.sql
cms-portal/html 这个在执行update-cms.sh的时候会把编译后的html放到这里
```
