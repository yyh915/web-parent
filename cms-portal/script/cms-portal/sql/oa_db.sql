/*
 Navicat Premium Data Transfer

 Source Server         : 106.13.140.154
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 106.13.140.154:3366
 Source Schema         : oa_db

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 02/02/2022 09:49:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_s_company
-- ----------------------------
DROP TABLE IF EXISTS `t_s_company`;
CREATE TABLE `t_s_company`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logo_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `created_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人id',
  `created_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `reg_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册时的ip',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公司' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_s_company
-- ----------------------------
INSERT INTO `t_s_company` VALUES (222222, '高河村', NULL, '2021-08-20 16:56:28', NULL, NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1412786441915269122, '测试公司333', 'jsdkfjaljfda', '2021-07-07 22:51:51', NULL, NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1412786697629401089, '公司测试', NULL, '2021-07-07 22:52:52', NULL, NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1412786814071668737, '哈哈哈公司', NULL, '2021-07-07 22:53:19', NULL, NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1428642930890440706, '就把袜子', NULL, '2021-08-20 16:59:52', NULL, NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1430544576896069633, '测试公司111', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1432723909265924098, '的房间艾克公司拟', 'sdfa', '2021-08-31 23:16:13', '2021-08-31 23:16:13', NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1433077420042342402, '点击发链接', NULL, '2021-09-01 22:40:57', '2021-09-01 22:40:57', NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1433430661238116354, '激发了了', NULL, '2021-09-02 22:04:36', '2021-09-02 22:04:36', NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1483792221852467202, '大搞和', NULL, '2022-01-19 21:23:28', '2022-01-19 21:23:28', NULL, NULL, NULL);
INSERT INTO `t_s_company` VALUES (1485260632274567170, '加我爱乐维', NULL, '2022-01-23 22:38:24', '2022-01-23 22:38:24', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_s_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_s_dict`;
CREATE TABLE `t_s_dict`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `category` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `sorts` int(3) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_s_dict
-- ----------------------------
INSERT INTO `t_s_dict` VALUES (111111, '系统默认管理员', '公司系统管理员', '2022-01-18 22:09:24', '2022-01-18 22:09:24', '123', NULL);
INSERT INTO `t_s_dict` VALUES (1434881973444042754, '葛洲坝', '123', '2021-09-06 22:11:36', '2021-09-06 22:11:36', '123', NULL);
INSERT INTO `t_s_dict` VALUES (1475111997229051906, '的shall飞角色', '阿打扫房间啊来得及朗了解到发了', '2021-12-26 22:31:21', '2021-12-26 22:31:21', '123', NULL);

-- ----------------------------
-- Table structure for t_s_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_s_resource`;
CREATE TABLE `t_s_resource`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `auth_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源权限标识',
  `parent_id` bigint(19) NOT NULL DEFAULT 0 COMMENT '父id',
  `type` tinyint(2) NULL DEFAULT NULL COMMENT '1:菜单，2：按钮，3：功能',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'api路径',
  `menu_sort` tinyint(2) NULL DEFAULT NULL COMMENT '排序',
  `status` tinyint(2) NULL DEFAULT 0 COMMENT '0：启用，1：禁用',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `descriptions` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `dependency_path` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '依赖其他api的路径',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_s_resource
-- ----------------------------
INSERT INTO `t_s_resource` VALUES (1479054353686880258, '系统管理', 'sysManage', 0, 1, NULL, NULL, 0, '2022-01-06 19:36:52', '2022-01-18 22:03:58', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1479054564857503745, '用户管理', 'userall', 1479054353686880258, 1, NULL, NULL, 0, '2022-01-06 19:37:42', '2022-01-06 19:37:42', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1479054863957516289, '角色管理', 'roleall', 1479054353686880258, 1, NULL, NULL, 0, '2022-01-06 19:38:54', '2022-01-06 19:39:06', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1479055271266377729, '公司管理', 'companyall', 1479054353686880258, 1, NULL, NULL, 0, '2022-01-06 19:40:31', '2022-01-06 19:40:31', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1479055343660064769, '菜单管理', 'menuall', 1479054353686880258, 1, NULL, NULL, 0, '2022-01-06 19:40:48', '2022-01-06 19:40:48', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1479057885299236866, '用户列表', 'userList', 1479054564857503745, 3, '/user/page-list', NULL, 0, '2022-01-06 19:50:54', '2022-01-19 22:16:53', '', '/user/detail/*,/role/page-list');
INSERT INTO `t_s_resource` VALUES (1479058028249505794, '用户添加', 'userAdd', 1479054564857503745, 2, '/user/save', NULL, 0, '2022-01-06 19:51:28', '2022-01-18 21:58:09', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1479058136173142018, '用户编辑', 'userEdit', 1479054564857503745, 2, '/user/save', NULL, 0, '2022-01-06 19:51:54', '2022-01-18 21:58:17', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1479058274123800577, '用户删除', 'userDel', 1479054564857503745, 2, '/user/del/*', NULL, 0, '2022-01-06 19:52:27', '2022-01-18 21:58:25', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481649610568015874, '角色列表', 'roleList', 1479054863957516289, 2, '/role/page-list', NULL, 0, '2022-01-13 23:29:30', '2022-01-18 21:48:48', NULL, '/role/detail/*');
INSERT INTO `t_s_resource` VALUES (1481649739333148674, '角色添加', 'roleAdd', 1479054863957516289, 2, '/role/save', NULL, 0, '2022-01-13 23:30:00', '2022-01-13 23:36:33', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481651496494845954, '角色修改', 'roleEidt', 1479054863957516289, 2, '/role/save', NULL, 0, '2022-01-13 23:36:59', '2022-01-13 23:36:59', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481651621422190593, '角色删除', 'roleDel', 1479054863957516289, 2, '/role/del/*', NULL, 0, '2022-01-13 23:37:29', '2022-01-18 21:48:33', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481651832609591297, '公司列表', 'companyList', 1479055271266377729, 2, '/company/page-list', NULL, 0, '2022-01-13 23:38:19', '2022-01-18 21:49:45', NULL, '/company/detail/*');
INSERT INTO `t_s_resource` VALUES (1481651948003282946, '公司修改', 'companyEdit', 1479055271266377729, 2, '/company/save', NULL, 0, '2022-01-13 23:38:47', '2022-01-13 23:38:47', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481652090794168321, '公司添加', 'companyAdd', 1479055271266377729, 2, '/company/save', NULL, 0, '2022-01-13 23:39:21', '2022-01-13 23:39:21', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481652230237999105, '公司删除', 'companyDel', 1479055271266377729, 2, '/company/del/*', NULL, 0, '2022-01-13 23:39:54', '2022-01-18 23:30:07', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481652397666226178, '菜单列表', 'resourceList', 1479055343660064769, 2, '/resource/list-tree', NULL, 0, '2022-01-13 23:40:34', '2022-01-13 23:40:34', NULL, '/resource/list/*');
INSERT INTO `t_s_resource` VALUES (1481652552507346945, '菜单添加', 'resourceAdd', 1479055343660064769, 2, '/resource/save', NULL, 0, '2022-01-13 23:41:11', '2022-01-13 23:41:11', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481652665191518209, '菜单修改', 'resourceEdit', 1479055343660064769, 2, '/resource/save', NULL, 0, '2022-01-13 23:41:38', '2022-01-13 23:41:38', NULL, NULL);
INSERT INTO `t_s_resource` VALUES (1481652795131056130, '菜单删除', 'resourceDel', 1479055343660064769, 2, '/resource/del/*', NULL, 0, '2022-01-13 23:42:09', '2022-01-18 23:30:19', NULL, NULL);

-- ----------------------------
-- Table structure for t_s_role
-- ----------------------------
DROP TABLE IF EXISTS `t_s_role`;
CREATE TABLE `t_s_role`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `company_id` bigint(19) NULL DEFAULT NULL COMMENT '公司关联',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_s_role
-- ----------------------------
INSERT INTO `t_s_role` VALUES (111111, '系统默认管理员', '公司系统管理员', '2022-01-18 22:09:24', '2022-01-18 22:09:24', 123);
INSERT INTO `t_s_role` VALUES (1434881973444042754, '葛洲坝', '123', '2021-09-06 22:11:36', '2021-09-06 22:11:36', 123);
INSERT INTO `t_s_role` VALUES (1475111997229051906, '的shall飞角色', '阿打扫房间啊来得及朗了解到发了', '2021-12-26 22:31:21', '2021-12-26 22:31:21', 123);

-- ----------------------------
-- Table structure for t_s_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_s_role_resource`;
CREATE TABLE `t_s_role_resource`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `resource_id` bigint(20) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色资源关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_s_role_resource
-- ----------------------------
INSERT INTO `t_s_role_resource` VALUES (1483635481177497601, 1434881973444042754, 1479058136173142018, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497602, 1434881973444042754, 1479054564857503745, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497603, 1434881973444042754, 1481649739333148674, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497604, 1434881973444042754, 1431471401134485506, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497605, 1434881973444042754, 1479057885299236866, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497606, 1434881973444042754, 1479054863957516289, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497607, 1434881973444042754, 1431471013916340225, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497608, 1434881973444042754, 1481649610568015874, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497609, 1434881973444042754, 1479058274123800577, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497610, 1434881973444042754, 1481652552507346945, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481177497611, 1434881973444042754, 1479054353686880258, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481252995074, 1434881973444042754, 1479055343660064769, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481252995075, 1434881973444042754, 1481651496494845954, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483635481252995076, 1434881973444042754, 1481651621422190593, '2022-01-19 11:00:38', '2022-01-19 11:00:38');
INSERT INTO `t_s_role_resource` VALUES (1483643287402307586, 1483441395426410497, 1481651832609591297, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890497, 1483441395426410497, 1481651948003282946, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890498, 1483441395426410497, 1481649610568015874, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890499, 1483441395426410497, 1479058274123800577, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890500, 1483441395426410497, 1481651496494845954, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890501, 1483441395426410497, 1481651621422190593, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890502, 1483441395426410497, 1479058136173142018, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890503, 1483441395426410497, 1481649739333148674, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890504, 1483441395426410497, 1481652090794168321, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890505, 1483441395426410497, 1479057885299236866, '2022-01-19 11:31:39', '2022-01-19 11:31:39');
INSERT INTO `t_s_role_resource` VALUES (1483643287414890506, 1483441395426410497, 1479054863957516289, '2022-01-19 11:31:39', '2022-01-19 11:31:39');

-- ----------------------------
-- Table structure for t_s_user
-- ----------------------------
DROP TABLE IF EXISTS `t_s_user`;
CREATE TABLE `t_s_user`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `company_id` bigint(19) NOT NULL COMMENT '哪个公司的用户',
  `department_id` bigint(19) NULL DEFAULT NULL COMMENT '部门Id',
  `admin_flag` tinyint(2) NULL DEFAULT NULL COMMENT '1:超级管理员，2:普通管理员，3：一般用户',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `phone` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `sex` tinyint(2) NULL DEFAULT NULL COMMENT '性别',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_s_user
-- ----------------------------
INSERT INTO `t_s_user` VALUES (1412786698132717570, 'admin', '$2a$10$pwAkGcwSkbvne940o.P95uEdo1ZEpOvWo.KBxKapApViWbM9orvuq', 1430544576896069633, NULL, 2, NULL, '哇偶', '1312342242', 'sfaksl@sfja.com', NULL, '2021-07-07 22:52:52', '2022-01-18 15:15:38');
INSERT INTO `t_s_user` VALUES (1412786814566596610, 'yyh', '$2a$10$2aXYY0cW12bWa92ds6UOT.xsLSZufyEvMXn3hp3YwHiQukGJcvOdm', 1430544576896069633, NULL, 1, NULL, '我', '23325', 'sdfa@sdf.com', NULL, '2021-07-07 22:53:20', '2022-01-18 19:45:06');
INSERT INTO `t_s_user` VALUES (1417109800721870849, 'yyh22', '222', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1417111788624900097, 'yyh33', '222', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1417112906306183170, 'yyh323', '222', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-19 21:23:40', NULL);
INSERT INTO `t_s_user` VALUES (1417113380400959490, 'yy2h323', '222', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-19 21:25:33', '2021-07-19 21:26:34');
INSERT INTO `t_s_user` VALUES (1430544577541992450, 'aaa', '$2a$10$tVS5rwvPPYqCM.PsiZToteOKFxxp2AVomxKLH.FZCC.iezYYzg.hi', 1430544576896069633, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1430915296503136257, 'aa1', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1430916992436404225, 'aa2', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1432348261225644034, 'adjflak', '$2a$10$ChKDifSt8DWgc1/.zpbbCuxi3sWseR22VOPR1KpJyROoqafTvP41m', 1430544576896069633, NULL, NULL, NULL, '十大垃圾', '1332235', 'sadf@sdf.com', NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1432349180424216578, 'sdfa', '$2a$10$B4c37yBNDSz2rHmcCuSnruRZL9Yzv9rRoYha4tJ/2unmazA3hcHda', 1430544576896069633, NULL, NULL, NULL, '收快递费', '132', 'sd@dsfs.com', NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1432349845875712001, 'fasd', '$2a$10$NAmVlORKdZAQbLftaQUNsedJwZygAMWiCYFIxybZvwuccVkcGDGLC', 1430544576896069633, NULL, NULL, NULL, '的咖啡', '233223', 'sfa@sdf.com', NULL, NULL, NULL);
INSERT INTO `t_s_user` VALUES (1432354914868256770, 'dfalj', '$2a$10$zOBvzXKwU86gKbB47.kHSuo.vhA7YcsBLxOT0Qw6VzvPYjzb2b8PS', 1430544576896069633, NULL, NULL, NULL, '手动蝶阀', '313133', 'sdfa@dfa.com', NULL, '2021-08-30 22:49:58', '2021-08-30 22:49:58');
INSERT INTO `t_s_user` VALUES (1433077420604379137, 'ufia', '$2a$10$yOxIJXt5FwYdz56UwoKtP.pULRKw/bQ7DvhYbxXzwkbiPRm/F7KOy', 1433077420042342402, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-09-01 22:40:57', '2021-09-01 22:40:57');
INSERT INTO `t_s_user` VALUES (1433430661720461313, 'dsfa', '$2a$10$2Ts7FR.FYRaPZP49U6mT3O9rWjL1.oDfYx5TJbXwaLnKpNKj1YlsG', 1433430661238116354, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-09-02 22:04:36', '2021-09-02 22:04:36');
INSERT INTO `t_s_user` VALUES (1483441608950038530, 'user', '$2a$10$2aXYY0cW12bWa92ds6UOT.xsLSZufyEvMXn3hp3YwHiQukGJcvOdm', 1430544576896069633, NULL, NULL, NULL, '测试user', NULL, NULL, NULL, '2022-01-18 22:10:15', '2022-01-18 22:10:15');
INSERT INTO `t_s_user` VALUES (1483792222401921025, 'admin1', '$2a$10$X3zzIxaJITU4RgkEU63YP.T.2mw7SYkYjAzQQKSE8GQppY12PsxWu', 1483792221852467202, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-01-19 21:23:28', '2022-01-19 21:23:28');
INSERT INTO `t_s_user` VALUES (1485260632798855169, 'add', '$2a$10$yzEDQRvO2Gj7l32nmhU1Qufr4NyLNHMjmp7iqAWd4fjQVBUbqIzuG', 1485260632274567170, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-01-23 22:38:24', '2022-01-23 22:38:24');

-- ----------------------------
-- Table structure for t_s_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_s_user_role`;
CREATE TABLE `t_s_user_role`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_s_user_role
-- ----------------------------
INSERT INTO `t_s_user_role` VALUES (1483337265928257537, 1412786698132717570, 1434881973444042754, '2022-01-18 15:15:38', '2022-01-18 15:15:38');
INSERT INTO `t_s_user_role` VALUES (1483405078667845634, 1412786814566596610, 1434881973444042754, '2022-01-18 19:45:06', '2022-01-18 19:45:06');
INSERT INTO `t_s_user_role` VALUES (1483441609080061954, 1483441608950038530, 1483441395426410497, '2022-01-18 22:10:15', '2022-01-18 22:10:15');

-- ----------------------------
-- Table structure for t_sg_customer
-- ----------------------------
DROP TABLE IF EXISTS `t_sg_customer`;
CREATE TABLE `t_sg_customer`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `open_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序用户唯一id',
  `access_token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `session_key` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `union_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信用户唯一id',
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sg_eatery
-- ----------------------------
DROP TABLE IF EXISTS `t_sg_eatery`;
CREATE TABLE `t_sg_eatery`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `del_flag` tinyint(1) NULL DEFAULT NULL COMMENT '1:删除',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `company_id` bigint(20) NULL DEFAULT NULL COMMENT '公司id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sg_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_sg_goods`;
CREATE TABLE `t_sg_goods`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '下单时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `company_id` bigint(20) NULL DEFAULT NULL COMMENT '公司id',
  `store_left` int(3) NULL DEFAULT NULL COMMENT '库存剩余',
  `init_store` int(2) NULL DEFAULT NULL COMMENT '商品库存，0无限',
  `category_id` bigint(20) NULL DEFAULT NULL COMMENT '商品i类别id',
  `gn_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '当前商品价格',
  `go_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '商品原价',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品描述',
  `status` int(2) NULL DEFAULT NULL COMMENT '1:上架，2：下架',
  `del_flag` tinyint(1) NULL DEFAULT NULL COMMENT '1:已删除 ,0:未删除',
  `sales` int(11) NULL DEFAULT NULL COMMENT '销量',
  `flavors` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '口味标签',
  `unit` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '单位',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sg_goods_category
-- ----------------------------
DROP TABLE IF EXISTS `t_sg_goods_category`;
CREATE TABLE `t_sg_goods_category`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `category_id` bigint(20) NULL DEFAULT NULL COMMENT '商品分类id',
  `sorts` int(3) NULL DEFAULT NULL COMMENT '排序',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '啥商品id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sg_goods_cc
-- ----------------------------
DROP TABLE IF EXISTS `t_sg_goods_cc`;
CREATE TABLE `t_sg_goods_cc`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `sorts` int(3) NULL DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '1:上架，2:下架',
  `del_flag` tinyint(1) NULL DEFAULT NULL COMMENT '1:删除',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `company_id` bigint(20) NULL DEFAULT NULL COMMENT '公司id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sg_order
-- ----------------------------
DROP TABLE IF EXISTS `t_sg_order`;
CREATE TABLE `t_sg_order`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '下单时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
  `company_id` bigint(20) NULL DEFAULT NULL COMMENT '公司id',
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `customer_id` bigint(20) NULL DEFAULT NULL COMMENT '顾客id',
  `order_status` int(1) NULL DEFAULT NULL COMMENT '0：未支付，11：支付中，12：支付成功,13：支付失败，4：取消支付，51：退款中，52：退款失败,53：退款款成功',
  `waite_no` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '排队号',
  `pay_type` int(1) NULL DEFAULT NULL COMMENT '支付类型，1：小程序支付',
  `pay_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付后第三方支付结果id',
  `customer_phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '顾客手机号',
  `order_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单备注',
  `pay_amount` decimal(10, 2) NOT NULL COMMENT '支付金额，支付后回调，和total_amount对比，如果不一致，订单就有问题',
  `total_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单总额',
  `goods_number` int(11) NULL DEFAULT NULL COMMENT '商品数量',
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '顾客手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sg_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_sg_order_detail`;
CREATE TABLE `t_sg_order_detail`  (
  `id` bigint(20) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '下单时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `company_id` bigint(20) NULL DEFAULT NULL COMMENT '公司id',
  `order_id` bigint(20) NULL DEFAULT NULL COMMENT '订单id',
  `customer_id` bigint(20) NULL DEFAULT NULL COMMENT '顾客id',
  `total_amount` decimal(10, 2) NOT NULL COMMENT '商品总金额',
  `goods_number` int(3) NULL DEFAULT NULL COMMENT '商品数量',
  `order_status` int(2) NULL DEFAULT NULL COMMENT '订单状态',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `gn_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '当前商品价格',
  `go_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '商品原价',
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '顾客手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
